#-----------------------------------------------------------------------------
# Name: Richard Vargas
# Purpose: Python Script to download "Loved" Songs from your Hypemachine 
# profile.
# DISCLAIMER - THIS PROGRAM IS FOR EDUCATIONAL PURPOSES ONLY.
# I wrote this as my 1st program in python ever, it's not very 
# good but it works and Python is awesome.
#
# Author: blitz2145
#
# Created:     11/08/2011
# Copyright:   (c) blitz2145 2011
# License:     MIT License
#-----------------------------------------------------------------------------
#!/usr/bin/env python
from BeautifulSoup import BeautifulSoup
import re
import requests
import simplejson
import urllib2

#Tweak this if the download is taking too long
#Or you want to grab less songs.
DOWN_LIM = 5

def get_hypem_json(url):
    #Get the raw JSON
    print 'Fetching Hypemachine JSON Data...'
    return requests.get(url)

def parse_pages(req_obj):
    #Grab the string of JSON from the request
    #Deserialize the json string
    playlist = simplejson.loads(req_obj.content)
    songs = []

    #Pull the Blog URL that a song is posted to
    #Tweak this range to pull-in more songs
    for i in range(0, 6):
        songs.append(playlist[str(i)]['posturl']);
    print 'Getting Page URLs with songs on them...'
    return songs

def fetch_pages(page_data):
    #Fetch the blog pages songs are on
    #This is very hacky, and should probably asynchronous, and error resilient
    songshtml = []

    for i in page_data:
        songshtml.append(requests.get(i))
    print 'Fetching pages...'
    return songshtml

def parse_mp3_files(songshtml):
    #Regex match URLs for those ending in .mp3
    songsfilepath = []

    for i in songshtml:
        if i.status_code == requests.codes.ok:
            soup = BeautifulSoup(i.content)
            parsedata = soup.findAll('a', href=re.compile('([^\s]+(\.(?i)(mp3))$)'))
            for x in parsedata:
                if x != None:
                    songsfilepath.append(x['href'])
    print 'Parsed out mp3 files.'
    return songsfilepath

def clean_file_paths(songsfilepath):
    #Remove relative and other incomplete paths
    #TODO: Complete these relative URLs to catch more songs
    for y in songsfilepath:
        if y.rfind('http://') == -1:
            songsfilepath.remove(y)
    #Encode spaces correctly in URLs.
    songsfilepath = [x.replace(' ', '%20') for x in songsfilepath][:DOWN_LIM]
    print 'Cleaned URLs.'
    return songsfilepath

def write_files(songspath):
    #Write out the files to the current directory in "#.mp3" format
    #TODO: Use Hypemachine's clean metadata to get decent filenames and
    #insert tag data into the files
    print 'Dowloading and Writing mp3\'s to disk (This might take a while)...'
    for y in songspath:
        try:
            mp3file = urllib2.urlopen(y)
            #Cleanup the filename a bit...
            name = y[y.rfind('/') + 1:].replace('%20', ' ')
            output = open(name, 'wb')
            output.write(mp3file.read())
            output.close()
        except urllib2.HTTPError, e:
            print 'The server couldn\'t fulfill the request.'
            print 'Error code: ', e.code
            continue
    print 'Done. Check your directory for the files'

def main():
    hypem_json = get_hypem_json('http://hypem.com/playlist/loved/blitz2145/json/1/data.js')
    page_data = parse_pages(hypem_json)
    song_data = fetch_pages(page_data)
    files_to_write = clean_file_paths(parse_mp3_files(song_data))
    write_files(files_to_write)

#Call the main function if this script is being run normally else its being 
#loaded as a module hence don't run main.
if __name__ == '__main__':
    main()